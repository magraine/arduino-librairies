
/*
  Moyenne.h - Librairie pour creer des moyennes
*/
#ifndef Moyenne_h
#define Moyenne_h

#include "Arduino.h"




class Moyenne
{
  public:
    Moyenne(int nombre_elements);
    int nb;
    int current;
    int total;
    int elements;
    int ajouter (int valeur);
};

#endif

