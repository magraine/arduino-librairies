//      Bouton.cpp
//      
//      Copyright 2011 Matthieu Marcillaud <marcimat@magraine.net>
//      
//      This program is free software; you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation; either version 2 of the License, or
//      (at your option) any later version.
//      
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//      GNU General Public License for more details.
//      
//      You should have received a copy of the GNU General Public License
//      along with this program; if not, write to the Free Software
//      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
//      MA 02110-1301, USA.
//      

#include "Arduino.h"
#include "Bouton.h"


// constructeurs
Bouton::Bouton () {
	Constructeur();
}


void Bouton::Constructeur () {
	delaySafeOn = delaySafeOff = BOUTON_SAFE_DELAY;
	delayClick = BOUTON_MULTICLICK_DELAY;
	delayHoldOn = delayHoldOff = BOUTON_HOLD_DELAY;
	firstOn = lastOn = lastClick = 0;
	firstOff = lastOff = millis();
	state = lastState = false;
	flagChangedOn = flagChangedOff = flagClick = false;
}


void Bouton::addValue(bool value){
	lastState = state;
	state = value;
	// etat different du precedent
	// on sauvegarde etat et timing
	if (state != lastState) {
		changedOn  = state;
		changedOff = !state;
		if (state) {
			firstOn  = millis();
			firstOff = lastOff = 0;
			flagClick = false;
		} else {
			firstOff = millis();
			firstOn  = lastOn = 0;
		}
	} else {
		changedOn = changedOff = false;
		// au bout d'un certain delai a un etat identique,
		// on passe en etat SafeOn et SafeOff
		if (state) {
			lastOn  = millis();
			if ((lastOn - firstOn) > delaySafeOn) {
				changedSafeOn = true;
				changedSafeOff = false;
				if (!flagClick) {
					flagClick = true;
					addClick();
				}
			}
		} else {
			lastOff = millis();
			if ((lastOff - firstOff) > delaySafeOff) {
				changedSafeOff = true;
				changedSafeOn = false;
			}
		}
	}
	
};

void Bouton::reset() {
	state = lastState = false;
	changedOn = changedOff = false;
	flagChangedOn = flagChangedOff = flagClick = false;
	firstOn = firstOff = lastOn = lastOff = 0;
	lastClick, click = 0;
}

bool Bouton::isOn(){
	return (state == true);
};


bool Bouton::isOff(){
	return (state == false);
};


bool Bouton::isSafeOn(){
	return changedSafeOn;
	
	// 2 mesures consecutives doivent etre identiques, sur ON !
	if (!state || !lastState) {
		return false;
	}

	// le delai ON doit etre depasse.
	return ((lastOn - firstOn) > delaySafeOn);	
};


bool Bouton::isSafeOff(){
	return changedSafeOff;
	
	// 2 mesures consecutives doivent etre identiques, sur OFF !
	if (state || lastState) {
		return false;
	}

	// le delai OFF doit etre depasse.
	return ((lastOff - firstOff) > delaySafeOff);
};


bool Bouton::isHoldOn(){
	// 2 mesures consecutives doivent etre identiques, sur ON !
	if (!state || !lastState) {
		return false;
	}

	// le delai HOLD ON doit etre depasse.
	return ((lastOn - firstOn) > delayHoldOn);		
};


bool Bouton::isHoldOff(){
	// 2 mesures consecutives doivent etre identiques, sur ON !
	if (!state || !lastState) {
		return false;
	}

	// le delai HOLD OFF doit etre depasse.
	return ((lastOff - firstOff) > delayHoldOn);
};


bool Bouton::hasChanged(){
	return (changedOn || changedOff);
};


bool Bouton::hasChangedOn(){
	return changedOn;
};


bool Bouton::hasChangedOff(){
	return changedOff;
};

// ne retourne true qu'une seule fois par ON
bool Bouton::hasChangedSafeOn(){
	if (isSafeOn()) {
		 if (!flagChangedOn) {
			return flagChangedOn = true;
		 }
	} else {
		flagChangedOn = false;
	}
	
	return false;
};

// ne retourne true qu'une seule fois par OFF
bool Bouton::hasChangedSafeOff(){
	if (isSafeOff()) {
		 if (!flagChangedOff) {
			return flagChangedOff = true;
		 }
	} else {
		flagChangedOff = false;
	}
	
	return false;
};


void Bouton::addClick() {
	unsigned long newClick = millis();
	if (newClick - lastClick < delayClick) {
		click++;
	} else {
		click = 1;
	}
	lastClick = millis();
}

int  Bouton::nbClick(){
	return click;
};
