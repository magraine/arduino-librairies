
/*
  LedRGB.h - Librairie pour controler des leds RGBs
*/
#ifndef LedRGB_h
#define LedRGB_h

#include "Arduino.h"
#include "Color.h"



// librairie pour piloter une led RGB
//
//  ||||
//   ||
//   |
//  r-gb
//
// LedRGB La = LedRGB(2,3,4);
//
// La.toColor(255,0,0);


class LedRGB
{
  public:
    LedRGB(int redPin, int greenPin, int bluePin);
    void color   (Color color);
    void color   (unsigned char red, unsigned char green, unsigned char blue);
    void toColor (unsigned char red, unsigned char green, unsigned char blue);
    void toColor (unsigned char red, unsigned char green, unsigned char blue, unsigned char interval);
	void toColor (Color color);
	void toColor (Color color, unsigned char interval);
	void blink (Color color_1, Color color_2);
	void blink (Color color_1, Color color_2, unsigned char interval); 
	void blink (Color color_1, Color color_2, unsigned char interval, unsigned char times);
  private:
    int _redPin, _greenPin, _bluePin;
    int _lastR, _lastG, _lastB;
    
};

#endif

