
/*
  Color.h - Librairie pour definir des couleurs
*/
#ifndef Chrono_h
#define Chrono_h

#include "Arduino.h"



class Chrono
{
  public:
    Chrono(void);
    void start();
    void stop();
    void reset();
    void pause();
    bool ready();
    bool after(unsigned long time);
    unsigned long get();
  private:
    unsigned long _start_time;
    unsigned long _pause_time;
    bool _is_paused;
};

#endif

