
/*
  Color.h - Librairie pour definir des couleurs
*/
#ifndef Color_h
#define Color_h

#include "Arduino.h"




class Color
{
  public:
    Color(int red, int green, int blue);
    int red, green, blue;
};

#endif

